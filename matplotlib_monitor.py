
import subprocess
import re
import datetime
import matplotlib.pyplot as plt 
from matplotlib.animation import FuncAnimation 

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
times , RSSIs = [] , []
  
def animate(frame,times,RSSIs):
    p = subprocess.Popen("netsh wlan show interfaces", stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out = p.stdout.read().decode()
    m = re.findall("Name.*?:.*?(?=\S*['-])([a-zA-Z'-]+).*?Signal.*?:.*?([0-9]*)%", out, re.DOTALL)
    p.communicate()
    
    RSSIs.append(int(m[0][1]))
    times.append(datetime.datetime.now().strftime('%H:%M:%S'))
    
    times = times[-100:]
    RSSIs = RSSIs[-100:]

    ax.clear()
    ax.plot(times,RSSIs)

    plt.xticks(rotation=45, ha='right')
    plt.subplots_adjust(bottom=0.30)
    plt.title("{}'s RSSI over time".format(m[0][0]))
    plt.ylabel('RSSI')
    plt.xlabel('Time')
   
    print("RSSIs",RSSIs)
    print("times",times)
    return ax,

animation=FuncAnimation(fig,animate,fargs=(times,RSSIs),interval=1000)
plt.show()