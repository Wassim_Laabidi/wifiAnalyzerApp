import subprocess
import re
import platform
def capture_WLAN():
	if platform.system() == 'Linux':
		p = subprocess.Popen("iwconfig", stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	elif platform.system() == 'Windows':
		p = subprocess.Popen("netsh wlan show networks mode=bssid ", stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	else:
		raise Exception('reached else of if statement')
		
	out = p.stdout.read().decode('unicode_escape').strip()
	print(out)
    

	if platform.system() == 'Linux':
		m = re.findall('(wlan[0-9]+)', out, re.DOTALL)
	elif platform.system() == 'Windows':	
		
		signal = re.findall("SSID [0-9]+ :.*?([a-zA-Z0-9_ -]+).*?", out, re.DOTALL)
		rssi = re.findall("Signal.*?:.*?([0-9]*)%", out, re.DOTALL)
		BSSID = re.findall("BSSID 1.*?:.*?((?:[0-9a-fA-F]:?){12})", out, re.DOTALL)
		Channel=re.findall("Channel.*?:.*?([0-9]+)", out, re.DOTALL)
	


	else:
		raise Exception('reached else of if statement')

	p.communicate()
	Channel=tuple(int(item) for item in Channel)
	rssi=tuple(int(item) for item in rssi)
	data=list(zip(signal,rssi,Channel,BSSID))
	
	return data

capture_WLAN()
	
