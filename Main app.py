import tkinter as tk
from tkinter import ttk
import time
import subprocess
from wifi_dispo import capture_WLAN

import tkinter as tk
from tkinter import ttk
import time
import subprocess
from wifi_dispo import capture_WLAN

def connect_to_strongest_signal(ssid):
    max_retries = 3
    ssid = ssid.strip()
    for _ in range(max_retries):
        try:
            result = subprocess.check_output(f'netsh wlan connect name="{ssid}"', shell=True)
            return result.decode('utf-8')
        except subprocess.CalledProcessError as e:
            print(f"Connection to {ssid} failed: {e.output.decode('utf-8')}. Retrying...")
            time.sleep(5)
    return f"Failed to connect to {ssid} after {max_retries} attempts."
def display_wlan_data():
    
    def refresh_wlan_data():
        wlan_data = capture_WLAN()
        wlan_data.sort(key=lambda x: x[1], reverse=True)
        for item in tree.get_children():
            tree.delete(item)
        for i, item in enumerate(wlan_data, start=1):
            if i == 1:
                tree.insert("", "end", values=item, tags=("strongest_signal",))
            elif i % 2 == 0:
                tree.tag_configure("evenrow", background="#E8E8E8")
                tree.insert("", "end", values=item, tags=("evenrow",))
            else:
                tree.insert("", "end", values=item)
        strongest_signal_label.config(text=f"Strongest Signal: {wlan_data[0][0]} (RSSI: {wlan_data[0][1]} dBm)")

    wlan_data = capture_WLAN()
    wlan_data.sort(key=lambda x: x[1], reverse=True)

    root = tk.Tk()
    root.title("WLAN Data")

    window_width = 800
    window_height = 400
    screen_width = root.winfo_screenwidth()
    screen_height = root.winfo_screenheight()
    x = (screen_width - window_width) // 2
    y = (screen_height - window_height) // 2
    root.geometry(f'{window_width}x{window_height}+{x}+{y}')

    style = ttk.Style()
    style.configure("Treeview.Heading", font=("Arial", 12))
    style.configure("Treeview", font=("Arial", 10), rowheight=30)

    tree = ttk.Treeview(root, columns=("SSID", "RSSI", "Channel", "BSSID"), show="headings")
    tree.heading("SSID", text="SSID")
    tree.heading("RSSI", text="RSSI")
    tree.heading("Channel", text="Channel")
    tree.heading("BSSID", text="BSSID")

    for i, item in enumerate(wlan_data, start=1):
        if i == 1:
            tree.insert("", "end", values=item, tags=("strongest_signal",))
        elif i % 2 == 0:
            tree.tag_configure("evenrow", background="#E8E8E8")
            tree.insert("", "end", values=item, tags=("evenrow",))
        else:
            tree.insert("", "end", values=item)

    tree.pack(fill="both", expand=True)

    strongest_signal_label = tk.Label(root, text=f"Strongest Signal: {wlan_data[0][0]} (RSSI: {wlan_data[0][1]} dBm)", font=("Arial", 14))
    strongest_signal_label.pack()
    strongest_signal_label.config(fg="green")
    tree.tag_configure("strongest_signal", background="green")

    refresh_button = tk.Button(root, text="Refresh", command=refresh_wlan_data)
    refresh_button.pack()

    connect_button = tk.Button(root, text="Connect to Strongest Signal", command=lambda: connect_and_print_result(root, wlan_data[0][0]))
    connect_button.pack()

    root.mainloop()

def connect_and_print_result(root, ssid):
    result = connect_to_strongest_signal(ssid)
    result_label = tk.Label(root, text=result, font=("Arial", 12))
    result_label.pack()

if __name__ == "__main__":
    display_wlan_data()
